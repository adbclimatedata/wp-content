<?php
/**
 * The sidebar containing the main widget area.
 *
 * @package MDLWP
 */

if ( is_active_sidebar( 'sidebar-1' ) && !is_bbpress() ) : ?>
		<?php dynamic_sidebar( 'sidebar-1' ); ?>
 
<?php elseif ( is_active_sidebar( 'bbp-sidebar' ) && is_bbpress() ) : ?>
 
     	<div id="secondary" class="widget-area" role="complementary">
    		<?php dynamic_sidebar( 'bbp-sidebar' ); ?>
    	</div><!-- .widget-area -->
 
<?php endif; ?>