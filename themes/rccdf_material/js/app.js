var file_root = site_url + '/uploads/files/';
var map = false;
var mapLocation = false;

function addMapLocation(){
	console.log('addMapLocation');
	// console.log(mapLocation);
	var geo_object = {
		display_name: mapLocation.original.formatted,
		latitude: mapLocation.lat,
        longitude: mapLocation.lon,
         extents: {
            western_extent: 0,
            eastern_extent: 0,
            northern_extent: 0,
            southern_extent: 0
         },
         address: {
            city: mapLocation.original.details.city,
            state: mapLocation.original.details.state,
            country_code: mapLocation.original.details.country_code,
            country: mapLocation.original.details.country
         }
	}
	console.log(geo_object);
	vm.addGeoEntryFromMap(geo_object);
	// vm.$data.geographic_coverage.push(geo_object);
}

jQuery( document ).ready(function() {
	console.log('doc ready');
    //map
	map = initMap(jQuery("#map"));
});

    
    var uploader = new qq.FineUploader({
        element: document.getElementById("uploader"),
        debug: true,
        request: {
	        endpoint: site_url + '/uploads/endpoint.php'
	    },
	    retry: {
	       enableAuto: true // defaults to false
	    },
	    deleteFile: {
	        enabled: true, // defaults to false
	        forceConfirm: true,
	        endpoint: site_url +  '/uploads/endpoint.php?'
	    },
	    callbacks:{
	    	onAllComplete: function(succeeded, failed) {
		    	// console.log('onAllComplete');
		     //    console.log(succeeded);
		     //    console.log(failed);
		       
		    },
		    onComplete: function(id, name, responseJSON, xhr) {
		    	console.log('onComplete');
		        
		        if (responseJSON.success == true ){
		        	console.log('UPLOAD SUCCESS');
		        	// console.log(responseJSON);
		        	var filepath = file_root + responseJSON.uuid + "/" + responseJSON.uploadName;
		        	console.log(filepath);
		        	vm.$data.assoicated_files.push(filepath);
		        }
		    },
		    onDeleteComplete: function(id, xhr, isError) {
		    	console.log('onDeleteComplete');
		        
		        if (!isError){
		        	console.log('DELETE SUCCESS');
		        	console.log(id);
		        	// console.log(xhr);
		        	// var filepath = file_root + responseJSON.uuid + "/" + responseJSON.uploadName;
		        	// console.log(filepath);
		        	vm.$data.assoicated_files.splice(id,1);

		        	vm.submit();
		        	uploader.reset();
		        	vm.populateFileUploadBox();
		        }
		    }
	    }
	    
	    
    });


	    



	function slugify(text){
	  return text.toString().toLowerCase()
	    .replace(/\s+/g, '-')           // Replace spaces with -
	    .replace(/[^\w\-]+/g, '')       // Remove all non-word chars
	    .replace(/\-\-+/g, '-')         // Replace multiple - with single -
	    .replace(/^-+/, '')             // Trim - from start of text
	    .replace(/-+$/, '');            // Trim - from end of text
	}

	var vm = new Vue({
		el: '#item-form-wrapper',
		data:{
			updated: false,
			update_message: '',
			container_id: uuid,
			database_id: 0,
			created_at:'',
			updated_at:'',
			title: '',
			slug: '',
			status: 'Draft',
			item_id: '',
			short_summary: '',
			publication_year: '',
			detailed_description: '',
			access_mechanism: [],
			content_owners: [
			],
			authors: [],
			assoicated_files: [],
			associated_agencies_or_organisations: [],
			associated_projects: [],
			contact_person: {
				email:'',
				address:'',
				mobile:0,
				name:'',
				phone:0,
			},
			
			content_storage_link: "",
			custodian: "",
			data_sharing_allowed: false,
			entered_by:{
				name: '',
				email: ''
			},
			format: 'pdf',
			keyword_descriptors: [],
			
			geographic_focus:[], //text array
			geographic_pin: [],
            geographic_path: [
                  ""
            ],
            geographic_coverage: [
            ],
			content_type: [],
			content_type_options:[
					"Audio/video",
		          	"Dataset",
		          	"Educational resource",
		          	"Image",
		          	"Presentation",
		          	"Publication",
		          	"Policy/Plan",
		          	"Software",
		          	"Tool",
		          	"Website"
			],
			relevant_sector_or_discipline_area: [],
			relevant_sector_or_discipline_area_options:[
					"Fisheries & Forestry",
		          	"Climate Science",
		          	"Climate Change Adaptation",
		          	"Climate Change Vulnerability & Impact Assessment",
		          	"Disaster Risk Management",
		          	"Education & Training",
		          	"Health & Social Development",
		          	"Human Settlements & Infrastructure",
		          	"Business & Industry",
		          	"Natural Resource Management & the Environment",
		          	"Economic Development"
			],
			keyword_descriptors_options:[
				  "AGRICULTURE Crops",
		          "AGRICULTURE Livestock",
		          "AGRICULTURE Horticulture",
		          "AGRICULTURE Irrigation",

		          "CLIMATE AND WEATHER Meteorology",
		          "CLIMATE AND WEATHER Climate change science",
		          "CLIMATE AND WEATHER Drought",
		          "CLIMATE AND WEATHER El Nino",
		          "CLIMATE AND WEATHER Extreme weather events",
		          "CLIMATE AND WEATHER Radiation",
		          "CLIMATE AND WEATHER Rainfall",
		          "CLIMATE AND WEATHER Adaptation",
		          "CLIMATE AND WEATHER Carbon",
		          "CLIMATE AND WEATHER Carbon Sequestration",
		          "CLIMATE AND WEATHER Climate models",
		          "CLIMATE AND WEATHER Climate projections",
		          "CLIMATE AND WEATHER Greenhouse gases",
		          "CLIMATE AND WEATHER Impacts",
		          "CLIMATE AND WEATHER Mitigation",
		          "CLIMATE AND WEATHER Resilience",
		          "CLIMATE AND WEATHER Risks",
		          "CLIMATE AND WEATHER Sea level rise",
		          "CLIMATE AND WEATHER Vulnerability",

		          "ECOLOGY Community",
		          "ECOLOGY Ecosystem",
		          "ECOLOGY Habitat",
		          "ECOLOGY Landscape",

		          "ENERGY Coal",
		          "ENERGY Electricity",
		          "ENERGY Petroleum",
		          "ENERGY Renewable",
		          "ENERGY Use",

		          "FAUNA Exotic",
		          "FAUNA Insects",
		          "FAUNA Invertebrates",
		          "FAUNA Native",
		          "FAUNA Vertebrates",

		          "FISHERIES Aquaculture",
		          "FISHERIES Freshwater",
		          "FISHERIES Marine",
		          "FISHERIES Recreational",

		          "FLORA Exotic",
		          "FLORA Native",

		          "FORESTS Agriforestry",
		          "FORESTS Natural",
		          "FORESTS Plantation",

		          "HAZARDS Cyclones",
		          "HAZARDS Drought",
		          "HAZARDS Earthquake",
		          "HAZARDS Fire",
		          "HAZARDS Flood",
		          "HAZARDS Landslip",
		          "HAZARDS Manmade",
		          "HAZARDS Pests",
		          "HAZARDS Severe local storms",
		          "HAZARDS Tsunamis",

		          "HUMAN ENVIRONMENT Economics",
		          "HUMAN ENVIRONMENT Housing",
		          "HUMAN ENVIRONMENT Livability",
		          "HUMAN ENVIRONMENT Planning",
		          "HUMAN ENVIRONMENT Structures and Facilities",
		          "HUMAN ENVIRONMENT Urban Design",

		          "INDUSTRY Manufacturing",
		          "INDUSTRY Mining",
		          "INDUSTRY Primary",
		          "INDUSTRY Service",
		          "INDUSTRY Other",

		          "LAND Cadastre",
		          "LAND Cover",
		          "LAND Geodesy",
		          "LAND Geography",
		          "LAND Ownership",
		          "LAND Topography",
		          "LAND Use",
		          "LAND Valuation",

		          "MARINE Biology",
		          "MARINE Coasts",
		          "MARINE Estuaries",
		          "MARINE Geology and Geophysics",
		          "MARINE Reefs",
		          "MARINE Human Impacts",
		          "MARINE Meteorology",
		          "MOLECULAR BIOLOGY Genetics",

		          "OCEANOGRAPHY Physical",
		          "OCEANOGRAPHY Chemical",

		          "PHOTOGRAPHY AND IMAGERY Aerial",
		          "PHOTOGRAPHY AND IMAGERY Remote Sensing",
		          "PHOTOGRAPHY AND IMAGERY Satellite",

		          "POLLUTION Air",
		          "POLLUTION Noise",
		          "POLLUTION Soil",
		          "POLLUTION Water",

		          "SOIL Erosion",
		          "SOIL Biology",
		          "SOIL Chemistry",
		          "SOIL Physics",

		          "TRANSPORTATION Air",
		          "TRANSPORTATION Land",
		          "TRANSPORTATION Marine",

		          "VEGETATION Floristic",
		          "VEGETATION Structural",

		          "WASTE Liquid",
		          "WASTE Solid",
		          "WASTE Toxic",
		          "WASTE Sewage",
		          "WASTE Greenhouse gas",
		          "WASTE Heat",

		          "WATER Groundwater",
		          "WATER Hydrology",
		          "WATER Hydrochemistry",
		          "WATER Lakes",
		          "WATER Rivers",
		          "WATER Salinity",
		          "WATER Supply",
		          "WATER Surface",
		          "WATER Quality",
		          "WATER Wetlands"
			]

		},
		ready: function(){
			console.log('Form Ready');
			if (item_id){
				this.getItemFromDatabase(item_id);
			}
			this.initComponents();
	      	// GET /someUrl
	      	// this.$http.get('http://203.101.230.216:6543/em-uri/d6f41668-ce83-4fb6-81bb-20ec5c0db679/9296f47e-00bc-4ef0-a1ff-835f812c71e8/metadata.json').then((response) => {
	       //    // success callback
	       //    console.log('success');
	       //    console.log(response);
	      	// }, (response) => {
	       //    // error callback
	       //    console.log('error');
	       //    console.log(response);
	      	// });

	    },
	    http: {
	      root: '/root',
	      headers: {
	        "Filename": "metadata.json",
	        "Content-Type": "application/json",
	        "Content-Disposition": "attachment; filename=metadata.json",
	        "Packaging": "http://purl.org/net/sword/package/Binary",
	        "In-Progress": "True",
	        "On-Behalf-Of": "arvesolland"
	      }
	    },
	    methods:{
	    	addContentOwner: function(){
	    		console.log('addContentOwner');
	    		this.$data.content_owners.push("");
	    		console.log(this.$data.content_owners);
	    	},
	    	removeContentOwner: function(index){
	    		console.log('removeContentOwner: ' + index);
	    		this.$data.content_owners.splice(index,1);
	    		console.log(this.$data.content_owners);	
	    	},

	    	addWebResource: function(){
	    		console.log('addWebResource');
	    		this.$data.access_mechanism.push("");
	    		console.log(this.$data.access_mechanism);
	    	},
	    	removeWebResource: function(index){
	    		console.log('removeWebResource: ' + index);
	    		this.$data.access_mechanism.splice(index,1);
	    		console.log(this.$data.access_mechanism);	
	    	},
	    	addAuthor: function(){
	    		console.log('addAuthor');
	    		this.$data.authors.push({name:'',email:''});
	    		console.log(this.$data.authors);
	    	},
	    	removeAuthor: function(index){
	    		console.log('removeAuthor: ' + index);
	    		this.$data.authors.splice(index,1);
	    		console.log(this.$data.authors);	
	    	},
	    	addProject: function(){
	    		console.log('addProject');
	    		this.$data.associated_projects.push({name:'',url:''});
	    		console.log(this.$data.associated_projects);
	    	},
	    	removeProject: function(index){
	    		console.log('removeProject: ' + index);
	    		this.$data.associated_projects.splice(index,1);
	    		console.log(this.$data.associated_projects);	
	    	},
	    	submit: function(){
	    		console.log('submit');
	    		if (this.validateForm()){
	    			console.log(this.convertToJson());	
	    			this.saveToDatabase();
	    		}
	    		
	    	},
	    	submitForm: function(){
	    		console.log('submitForm,');
	    		// if (this.validateForm()){
	    		// 	console.log(this.convertToJson());	
	    		// 	this.saveToDatabase();
	    		// }
	    		
	    	},
	    	convertToJson: function(){
	    		return JSON.stringify(this.createMetadataObjectForElastic());
	    	},
	    	createMetadataObjectForElastic: function(){

	    		var elastic_data = {};
	    		elastic_data['title'] = this.$data.title;
	    		elastic_data['database_id'] = this.$data.database_id;
	    		elastic_data['container_id'] = this.$data.container_id;
	    		elastic_data['status'] = this.$data.status;
	    		elastic_data['slug'] = slugify(this.$data.title);
	    		elastic_data['item-id'] = slugify(this.$data.item_id);
	    		elastic_data['short-summary'] = this.$data.short_summary;
	    		elastic_data['detailed-description'] = this.$data.detailed_description;
	    		
	    		elastic_data['publication-year'] = this.$data.publication_year;
	    		elastic_data['access-mechanism'] = this.$data.access_mechanism;
	    		elastic_data['content-owners'] = this.$data.content_owners;
	    		elastic_data['authors'] = this.$data.authors;
	    		elastic_data['assoicated-files'] = this.$data.assoicated_files;
	    		elastic_data['associated-projects'] = this.$data.associated_projects;
	    		elastic_data['associated-agencies-or-organisations'] = this.$data.associated_agencies_or_organisations;
	    		elastic_data['contact-person'] = this.$data.contact_person;
	    		elastic_data['content-storage-link'] = this.$data.content_storage_link;
	    		elastic_data['content-type'] = this.$data.content_type;
	    		elastic_data['custodian'] = this.$data.custodian;
	    		elastic_data['data-sharing-allowed'] = this.$data.data_sharing_allowed;
	    		elastic_data['entered-by'] = this.$data.entered_by;
	    		elastic_data['format'] = this.$data.format;
	    		elastic_data['keyword-descriptors'] = this.$data.keyword_descriptors;
	    		elastic_data['relevant-sector-or-discipline-area'] = this.$data.relevant_sector_or_discipline_area;
	    		
	    		
	    		elastic_data['geographic-pin'] = [];
	    		elastic_data['geographic-path'] = [];
	    		elastic_data['geographic-focus'] = [];

	    		var geo_path = [];
	    		var ordered_place_names = ["continent", "country", "state", "province",
                       "district", "county", "region", "municipality",
                       "city", "borough", "suburb", "quarter",
                       "neighbourhood", "city_block", "plot",
                       "town", "village", "hamlet", "isolated_dwelling",
                       "farm", "allotments", "archipelago",
                       "island", "islet", "locality"];
               
	    		
	    		var geo_coverage = [];
	    		for (var i = 0, len = this.$data.geographic_coverage.length; i < len; i++) {
	    			//do path calculation for this location
	    			var path = '';
	    			for(name in ordered_place_names){
	    				
	                	if(typeof(this.$data.geographic_coverage[i].address[ordered_place_names[name]]) !== 'undefined'){
	                		if(this.$data.geographic_coverage[i].address[ordered_place_names[name]].length){
	                			console.log(this.$data.geographic_coverage[i].address[ordered_place_names[name]]);
	                			path += "/" + this.$data.geographic_coverage[i].address[ordered_place_names[name]];	
	                		}
	                		
	                	}
	                }
	                if(path.length){
	                	geo_path.push(path);	
	                }
	                

	    			var geo_object = {
		    			'display-name': this.$data.geographic_coverage[i].display_name,
		    			latitude: this.$data.geographic_coverage[i].latitude,
	                    longitude: this.$data.geographic_coverage[i].longitude,
	                     extents: {
	                        'western-extent': this.$data.geographic_coverage[i].extents.western_extent,
	                        'eastern-extent': this.$data.geographic_coverage[i].extents.eastern_extent,
	                        'northern-extent': this.$data.geographic_coverage[i].extents.northern_extent,
	                        'southern-extent': this.$data.geographic_coverage[i].extents.southern_extent
	                     },
	                     address: {
	                        'city': this.$data.geographic_coverage[i].address.city,
	                        'state': this.$data.geographic_coverage[i].address.state,
	                        'country_code': this.$data.geographic_coverage[i].address.country_code,
	                        'country': this.$data.geographic_coverage[i].address.country
	                     }
		    		}
		    		geo_coverage.push(geo_object);
		    		
		    		elastic_data['geographic-path'].push(geo_object['display-name']);
		    		elastic_data['geographic-focus'].push(this.$data.geographic_coverage[i].display_name);

		    		var pin_object = {
		    			// location: [this.$data.geographic_coverage[i].longitude,this.$data.geographic_coverage[i].latitude]
		    			location:{
		    				lat: this.$data.geographic_coverage[i].latitude,
		    				lon: this.$data.geographic_coverage[i].longitude
		    			}
		    		};
		    		elastic_data['geographic-pin'].push(pin_object);

	    		}
	    		elastic_data['geographic-coverage'] = geo_coverage;

	    		if (geo_path.length){
	    			elastic_data['geographic-path'] = geo_path;	
	    		} 
	    		this.$data.geographic_path = elastic_data['geographic-path'];
	    		this.$data.geographic_focus = elastic_data['geographic-focus'];
	    		
                // var elastic_data = this.$data;     
	    		return elastic_data;
	    	},
	    	replaceUnderscoreKeysWithDashes: function(object){
	    		console.log('replaceUnderscoreKeysWithDashes');
	    		for (var property in object) {
				    if (object.hasOwnProperty(property)) {
				        console.log(property);
				        property.replace('_','-');

				    } 
				    if( typeof property === 'object' ){
				    	property = this.replaceUnderscoreKeysWithDashes(property);
				    }
			    
				    
				}
				return object;
	    	},
	    	replaceDashesKeysWithUnderscores: function(object){
	    		for (var property in object) {
				    if (object.hasOwnProperty(property)) {
				        console.log(property);
				        property.replace('-','_');
				    }
				}
				return object;
	    	},
	    	validateForm: function(){
	    		console.log('Validate form');
	    		return true;
	    	},
	    	saveToDatabase: function(){
	    		console.log('saveToDatabase');
	    		var self = this;
	    		 this.$http.post(ajaxurl, { metadata: self.convertToJson(),action: 'repository_add_item' }, { emulateJSON: true }).then(function(response) {
	    		 		console.log('success');
                        console.log( response.json() );
                        if (response.json().status == 'success'){
                        	self.$data.database_id = response.json().id;
                        }
                        
                        self.markasUpdated(response.json().message);
                    }, function(response) {
                        // error callback
                        console.log('error');
                       self.markasUpdated(response.json().message);
                    });
	    		
	    	},
	    	populateItem: function(db_item){
	    		console.log('populateItem');
	    		//var db_item = this.getItemFromDatabase(item_id);

	    		console.log(db_item);
	    		if (db_item){
	    			this.$data.title = db_item['title'];
		    		this.$data.database_id = db_item['database_id'];
		    		this.$data.container_id = db_item['container_id'];
		    		this.$data.status = db_item['status'];
		    		this.$data.slug = db_item['slug'];
		    		this.$data.item_id = db_item['item-id'];
		    		this.$data.short_summary = db_item['short-summary'];
		    		this.$data.detailed_description = db_item['detailed-description'];
		    		this.$data.publication_year = db_item['publication-year'];
		    		this.$data.access_mechanism = db_item['access-mechanism'];
		    		this.$data.content_owners = db_item['content-owners'];
		    		this.$data.authors = db_item['authors'];
		    		this.$data.assoicated_files = db_item['assoicated-files'];
		    		this.$data.associated_projects = db_item['associated-projects'];
		    		this.$data.associated_agencies_or_organisations = db_item['associated-agencies-or-organisations'];
		    		this.$data.contact_person = db_item['contact_person'];
		    		this.$data.content_storage_link = db_item['content-storage-link'];
		    		this.$data.content_type = db_item['content-type'];
		    		this.$data.custodian = db_item['custodian'];
		    		this.$data.data_sharing_allowed = db_item['data-sharing-allowed'];
		    		this.$data.entered_by = db_item['entered-by'];
		    		this.$data.format = db_item['format'];
		    		this.$data.keyword_descriptors = db_item['keyword-descriptors'];
		    		this.$data.relevant_sector_or_discipline_area = db_item['relevant-sector-or-discipline-area'];
		    		// this.$data.geographic_focus = db_item['geographic-focus'];
		    		this.$data.created_at = db_item['created_at'];
		    		this.$data.updated_at = db_item['updated_at'];

		    		this.$data.geographic_focus = db_item['geographic-focus'];
		    		this.$data.geographic_pin = db_item['geographic-pin'];
		    		this.$data.geographic_path = db_item['geographic-path'];
		    		
		    		var geo_coverage = [];
		    		for (var i = 0, len = db_item['geographic-coverage'].length; i < len; i++) {
		    			var geo_object = {
			    			display_name: db_item['geographic-coverage'][i]['display-name'],
			    			latitude: db_item['geographic-coverage'][i].latitude,
		                    longitude: db_item['geographic-coverage'][i].longitude,
		                    extents: {
		                        western_extent: db_item['geographic-coverage'][i].extents['western-extent'],
		                        eastern_extent: db_item['geographic-coverage'][i].extents['eastern-extent'],
		                        northern_extent: db_item['geographic-coverage'][i].extents['northern-extent'],
		                        southern_extent: db_item['geographic-coverage'][i].extents['southern-extent']
		                     },
		                     address: {
		                        city: db_item['geographic-coverage'][i].address.city,
		                        state: db_item['geographic-coverage'][i].address.state,
		                        country_code: db_item['geographic-coverage'][i].address['country_code'],
		                        country: db_item['geographic-coverage'][i].address.country
		                     }
			    		}
			    		geo_coverage.push(geo_object);
		    		}
		    		this.$data.geographic_coverage = geo_coverage;
		    		


		    		this.initComponents();
	    		}
	    	},
	    	getItemFromDatabase: function(item_id){
	    		console.log('getItemFromDatabase: '+item_id);	
	    		var self = this;
	    		this.$http.post(ajaxurl, { item_id: item_id,action: 'repository_get_item' }, { emulateJSON: true }).then(function(response) {
    		 		console.log('success');
    		 		var json_item = JSON.parse(response.json().item.metadata_json)
    		 		if (!json_item.database_id){
    		 			json_item.database_id = item_id;
    		 		}
                    self.populateItem(json_item);

                }, function(response) {
                    // error callback
                    console.log('error');
                });

	    	},
	    	initComponents: function(){
	    		var self = this;
	    		//content type
				var content_type_select = jQuery("#content_type_select").val(self.$data.content_type).select2().on('change', function () {
			        self.$data.content_type = jQuery(this).val();
			    });
				// keyword_descriptors
				var keyword_descriptors_select = jQuery("#keyword_descriptors_select").val(self.$data.keyword_descriptors).select2().on('change', function () {
			        self.$data.keyword_descriptors = jQuery(this).val();
			    });
			    // relevant_sector_or_discipline_area
			    console.log('relevant_sector_or_discipline_area:'+self.$data.relevant_sector_or_discipline_area);
			    var relevant_sector_or_discipline_area_select = jQuery("#relevant_sector_or_discipline_area_select").val(self.$data.relevant_sector_or_discipline_area).select2().on('change', function () {
			        self.$data.relevant_sector_or_discipline_area = jQuery(this).val();
			    });

			    this.populateFileUploadBox();

			    
	    	},
	    	populateFileUploadBox: function(){
	    		//files
			    var file_array = this.convertFilepathsToObjects();
			    uploader.addInitialFiles(file_array);
	    	},
	    	convertFilepathsToObjects: function(){
	    		console.log('convertFilepathsToObjects');
	    		var files = [];
	    		for (var i = 0, len = this.$data.assoicated_files.length; i < len; i++) {
	    			var path = this.$data.assoicated_files[i];
	    			console.log(path);
	    			path = path.replace(file_root,"");
	    			var fileinfo = path.split("/"); 

	    			var file_object = {};
	    			file_object.uuid = fileinfo[0];
	    			file_object.name = fileinfo[1];

	    			files.push(file_object);
	    			// console.log(file_object);
				}
				return files;
	    		
	    	},
	    	markasUpdated: function(msg){
	    		var self = this;
	    		
	    		console.log('markasUpdated: ' + msg);
	    		//jQuery('#submitresult').html('dfiiiiiii');
	    		this.$data.update_message = msg;
	    		this.$data.updated = true;
	    		setTimeout(function(){
	                self.$data.updated = false;
	            },4000);
	    	},
	    	addGeoEntry: function(){
	    		console.log('addGeoEntry');
	    		var geo_object = {
	    			display_name: "",
	    			latitude: "",
                    longitude: "",
                     extents: {
                        western_extent: 0,
                        eastern_extent: 0,
                        northern_extent: 0,
                        southern_extent: 0
                     },
                     address: {
                        city: "",
                        state: "",
                        country_code: "",
                        country: ""
                     }
	    		}
	    		this.$data.geographic_coverage.push(geo_object);
	    		console.log(this.$data.geographic_coverage);
	    	},
	    	addGeoEntryFromMap: function(geo_object){
	    		console.log('addGeoEntryFromMap');
	    		this.$data.geographic_coverage.push(geo_object);
	    		console.log(this.$data.geographic_coverage);
	    	},
	    	removeGeoEntry: function(index){
	    		console.log('removeGeoEntry: ' + index);
	    		this.$data.geographic_coverage.splice(index,1);
	    		console.log(this.$data.geographic_coverage);	
	    	}
	    	// addMapLocation: function(){
	    	// 	console.log('addMapLocation');
	    	// 	console.log(mapLocation);
	    	// }


	    }

	})