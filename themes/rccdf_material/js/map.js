// jQuery( document ).ready(function() {
//   console.log('doc ready');
// (function(win, doc){
//   'use strict';
//   console.log('map debug 1');
//   var olview = new ol.View({
//         center: [0, 0],
//         zoom: 2,
//         minZoom: 2,
//         maxZoom: 20
//       }),
//       baseLayer = new ol.layer.Tile({
//         source: new ol.source.OSM()
//       }),
//       map = new ol.Map({
//         target: doc.getElementById('map'),
//         view: olview,
//         layers: [baseLayer]
//       });
//   console.log(map);

//   //Instantiate with some options and add the Control
//   var geocoder = new Geocoder('nominatim', {
//     provider: 'osm',
//     lang: 'en',
//     placeholder: 'Search for ...',
//     limit: 5,
//     debug: true,
//     autoComplete: true,
//     keepOpen: true
//   });
//   map.addControl(geocoder);
  
//   //Listen when an address is chosen
//   geocoder.on('addresschosen', function(evt){
//     var feature = evt.feature;
//     var coord = evt.coordinate;
//     var address_html = feature.get('address_html');
//     console.log(evt);
//     content.innerHTML = '<p>'+ address_html +'</p>';
//     overlay.setPosition(coord);
//   });

//   /**
//   * Popup
//   **/
//   var container = doc.getElementById('popup'),
//       content = doc.getElementById('popup-content'),
//       closer = doc.getElementById('popup-closer'),
//       overlay = new ol.Overlay({
//         element: container,
//         offset: [0, -40]
//       });
//   closer.onclick = function() {
//     overlay.setPosition(undefined);
//     closer.blur();
//     return false;
//   };
//   map.addOverlay(overlay);
//   console.log('map debug 2');

//   map.updateSize();

// })(window, document);

// });

function initMap(el){
    console.log(el);
    var drawing = false;

    var drawControl = function(opt_options) {

        var options = opt_options || {};

        var button = document.createElement('button');
        button.innerHTML = '<i class="fa fa-crop"></i>';
        button.type = 'button';

        var this_ = this;
        
       


        button.addEventListener('click', function(){
            map.removeInteraction(polygonInteraction);
            drawing = true;
            drawPoly(jQuery(this));
        });

        var element = document.createElement('div');
        element.className = 'draw-poly ol-unselectable ol-control';
        element.appendChild(button);

        ol.control.Control.call(this, {
          element: element,
          target: options.target
        });

    };

    var image = new ol.style.Circle({
        radius: 4,
        fill: new ol.style.Fill({color: [0,0,0,1] }),
        stroke: new ol.style.Stroke({
            color: '#ffcc33', width: 3
        })
    });

    var imageHighlight = new ol.style.Circle({
        radius: 4,
        fill: new ol.style.Fill({color: [0,0,0,1] }),
        stroke: new ol.style.Stroke({
            color: '#ffcc33', width: 3
        })
    });

    var featureStyles = {
        'Point': new ol.style.Style({
          image: image
        }),
        'LineString': new ol.style.Style({
          stroke: new ol.style.Stroke({
            color: 'green',
            width: 1
          })
        }),
        'MultiLineString': new ol.style.Style({
          stroke: new ol.style.Stroke({
            color: 'green',
            width: 1
          })
        }),
        'MultiPoint': new ol.style.Style({
          image: image
        }),
        'MultiPolygon': new ol.style.Style({
              stroke: new ol.style.Stroke({
                color: 'rgba(0, 204, 102,.6)',
                width: 1
              }),
              fill: new ol.style.Fill({
                color: 'rgba(0, 204, 102, .2)'
              })
        }),
        'Polygon': new ol.style.Style({
          stroke: new ol.style.Stroke({
            color: 'blue',
            lineDash: [4],
            width: 1
          }),
          fill: new ol.style.Fill({
            color: 'rgba(0, 0, 255, 0.1)'
          })
        }),
        'GeometryCollection': new ol.style.Style({
          stroke: new ol.style.Stroke({
            color: 'magenta',
            width: 1
          }),
          fill: new ol.style.Fill({
            color: 'magenta'
          }),
          image: new ol.style.Circle({
            radius: 10,
            fill: null,
            stroke: new ol.style.Stroke({
              color: 'magenta'
            })
          })
        }),
        'Circle': new ol.style.Style({
          stroke: new ol.style.Stroke({
            color: 'red',
            width: 2
          }),
          fill: new ol.style.Fill({
            color: 'rgba(255,0,0,0.2)'
          })
        })
    };

    var multiPolygonHoverStyle = new ol.style.Style({
        stroke: new ol.style.Stroke({
            color: 'rgba(255, 204, 51, 1)',
            width: 1
        }),
        fill: new ol.style.Fill({
            color: 'rgba(255, 204, 51, .5)'
        })
    });

    var clusterHoverStyle = function(feature){
        var size = feature.get('features').length;
        return new ol.style.Style({
            image: new ol.style.Circle({
              radius: 10,
              fill: new ol.style.Fill({
                color: '#000'
              })
            }),
            text: new ol.style.Text({
              text: size.toString(),
              fill: new ol.style.Fill({
                color: '#ffcc33'
              }),
              stroke: new ol.style.Stroke({
                  color: '#ffcc33',
                  width: 1
              })
            })
          })
    }

    var extentFeatures = new ol.Collection();
    var extentOverlay = new ol.layer.Vector({
        source: new ol.source.Vector({features: extentFeatures}),
        name: 'DrawFeatures',
        title: 'DrawFeatures',
        style: new ol.style.Style({
            fill: new ol.style.Fill({
              color: 'rgba(255, 255, 255, 0.25)'
            }),
            stroke: new ol.style.Stroke({
              color: '#80b3ff',
              width: 2,
              lineDash: [10,10]
            })
        })
    });

    var styleFunction = function(feature) {
        return featureStyles[feature.getGeometry().getType()];
    };
  
    ol.inherits(drawControl, ol.control.Control);

    var styleCache = {};
    
   
   

    var features = new ol.Collection();
    var featureOverlay = new ol.layer.Vector({
        source: new ol.source.Vector({features: features}),
        name: 'Features',
        title: 'Features',
        style: styleFunction,
        visible: true
    });

  var mapId = el.attr('id');

  var mapboxKey = 'pk.eyJ1IjoiYXJ2ZXNvbGxhbmQiLCJhIjoiY2lyeTk2YnpxMDBlOTJ5bzRlZnRvdG9zciJ9.E4_LKLYrKhSPNm63lSQ1dg';
  
  var map = new ol.Map({
    layers: [
        new ol.layer.Tile({
          title: 'Mapbox',
          type: 'base',
          visible: true,
          source: new ol.source.XYZ({
            tileSize: [512, 512],
            url: 'https://api.mapbox.com/styles/v1/mapbox/streets-v8/tiles/{z}/{x}/{y}?access_token='+mapboxKey+''
          })
        }),
    ],
    controls: ol.control.defaults({
        //attributionOptions: /** @type {olx.control.AttributionOptions} */ ({
        //    collapsible: false
        //})
    }).extend([
        new drawControl()
    ]),
    target: document.getElementById(''+mapId+''),
    view: new ol.View({
            center: ol.proj.transform([130, 0], 'EPSG:4326', 'EPSG:3857'),
            zoom: 4,
            minZoom:2,
        })
    });

    var popup = new ol.Overlay.Popup({"panMapIfOutOfView":false});
    map.addOverlay(popup);

    var polygonInteraction = new ol.interaction.Select({
        condition: ol.events.condition.pointerMove,
        layers:  [featureOverlay],
        style: multiPolygonHoverStyle
    });


   map.addInteraction(polygonInteraction);

   
    var drawPoly = function(el){
        var draw;

        var geometryFunction = function(coordinates, geometry) {
             if (!geometry) {
                 geometry = new ol.geom.Polygon(null);
             }
             var start = coordinates[0];
             var end = coordinates[1];
             geometry.setCoordinates([
                 [start, [start[0], end[1]], end, [end[0], start[1]], start]
             ]);
             return geometry;
        };

        draw = new ol.interaction.Draw({
             source: extentOverlay.getSource(),
             type: /** @type {ol.geom.GeometryType} */ 'LineString',
             geometryFunction: geometryFunction,
             maxPoints: 2
        });

        draw.on('drawstart', function(evt){
             // this isn't being used... yet
             
        });

        draw.on('drawend', function(evt){
             evt.feature.setId('geo_constraints');

             // interaction finished, free up mouse events
             map.removeInteraction(draw);
             map.addInteraction(polygonInteraction);
             // map.addInteraction(clusterInteraction);
             drawing = false;

             var geom = evt.feature.getGeometry();
             var ext = geom.getExtent();
             var mapProj = map.getView().getProjection().getCode();
             var transfn = ol.proj.getTransform(mapProj, 'EPSG:4326');
             var newext = ol.extent.applyTransform(ext, transfn);

             jQuery('#north_bounds').val(newext[3].toFixed(6));
             jQuery('#east_bounds').val(newext[2].toFixed(6));
             jQuery('#south_bounds').val(newext[1].toFixed(6));
             jQuery('#west_bounds').val(newext[0].toFixed(6));

             jQuery('#north_bounds, #east_bounds, #south_bounds, #west_bounds').parent('.mdl-textfield').addClass('is-dirty');

             //console.log(evt.feature.getGeometry().getExtent());

             map.getView().fit(evt.feature.getGeometry().getExtent(), map.getSize(), {padding: [50,50,50,50]});

        });

        map.addInteraction(draw);
    }

    /**
    * Popup
    **/
    var container = document.getElementById('popup'),
        content = document.getElementById('popup-content'),
        closer = document.getElementById('popup-closer'),
        overlay = new ol.Overlay({
          element: container,
          offset: [0, -40]
        });
    closer.onclick = function() {
      overlay.setPosition(undefined);
      closer.blur();
      return false;
    };
    map.addOverlay(overlay);

    var popup = new ol.Overlay.Popup();
    map.addOverlay(popup);

    var geocoder = new Geocoder('nominatim', {
        // provider: 'osm',
        provider: 'osm',
        key: '',
        lang: 'en-US', //en-US, fr-FR
        placeholder: 'Search for places...',
        limit: 5,
        debug:false,
        keepOpen: false,
        autoComplete: true,
        preventDefault: true
    });

    map.addControl(geocoder);

    geocoder.on('addresschosen', function(evt){
        console.log(evt);
        var
          feature = evt.feature,
          coord = evt.coordinate,
          address_html = feature.get('address_html')
        ;
        var loc = {};
        loc.feature = feature.get('address_original');
        var original_address = feature.get('address_original');
        loc.coord = coord;
        mapLocation = original_address;
        // console.log(feature);
        // popup.show(evt.coordinate, address_html);

        var addHtml = '<button onClick="addMapLocation()" class="btn btn-primary" type="button">Add This Location</button>';
        content.innerHTML = '<p>'+address_html+'</p><p>'+addHtml+'</p>';
        overlay.setPosition(coord);
        jQuery('#popup').show();
        // overlay.show();

    });

    jQuery('#input-bounds').click(function(e){
        e.preventDefault();

        // get coordinates
        var coords = {}

        jQuery('#coord-inputs').find('input[type="text"]').each(function(){
          var name = jQuery(this).attr('name');
          var value = jQuery(this).val();
              value = parseFloat(value);
          coords[name] = value;
        });

        // get rid of any existing bounds
        extentOverlay.getSource().clear();

        var bounds = [
             [coords.west, coords.north], 
             [coords.east, coords.north], 
             [coords.east, coords.south], 
             [coords.west, coords.south],
             [coords.west, coords.north]
        ];

        var polygon = new ol.geom.Polygon([bounds]);
        var mapProj = map.getView().getProjection().getCode();
        polygon.transform('EPSG:4326', mapProj);

       

  });

 

  // paste the map object into the dom so it can be referenced
  jQuery(map.getTargetElement()).data('map', map);

  jQuery('#geo').removeClass('active');

  return map;
}

function getLayerByTitle(map, title){
    var layer;
    map.getLayers().forEach(function(lyr) {
        if (!(lyr instanceof ol.layer.Group)) {
            if (lyr.get('title') == title){
                layer = lyr;
            }
        } 
    });
    return layer
}

function getMap(){
    return jQuery('#map').data('map');
}