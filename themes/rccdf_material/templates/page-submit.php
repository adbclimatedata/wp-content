<?php
/**
 *
 * Template Name: Page Submit Form
 *
 *
 * @package MDLWP
 */

get_header(); ?>

<style>


.page-template-page-submit .site-main {
	padding-top: 0;

	
}

.page-template-page-submit .site-main header h3 {
		color: #3e6371;
		margin-bottom: 1em;
		font-size: 3em;
	}	

.page-template-page-submit	.mdlwp-page-ribbon{
	margin-top: -35vh;
	flex-shrink: 0;
	display: block;
	overflow-y: auto;
	overflow-x: hidden;
	flex-grow: 1;
	z-index: 1;
	-webkit-overflow-scrolling: touch;
}

.page-template-page-submit	.ribbon {
	width: 100%;
	height: 40vh;
	flex-shrink: 0;
	background-position: center;
	background-size: cover;
}

.page-template-page-submit	.ribbon-content {
	padding: 40px 60px;
}

.page-template-page-submit	.is-small-screen {
	.ribbon-content {
		padding: 20px 30px;
	}
}




</style>

	
	<div id="primary" class="content-area">
		<main id="main" class="site-main" role="main">

			<?php do_action( 'mdlwp_before_content' ); ?>

			<?php while ( have_posts() ) : the_post(); ?>

				<?php get_template_part( 'template-parts/content', 'submit' ); ?>

				<?php do_action( 'mdlwp_before_comments' ); ?>

				<?php
					// If comments are open or we have at least one comment, load up the comment template.
					if ( comments_open() || get_comments_number() ) :
						comments_template();
					endif;
				?>

				<?php do_action( 'mdlwp_after_comments' ); ?>

			<?php endwhile; // End of the loop. ?>

			<?php do_action( 'mdlwp_after_content' ); ?>

		</main><!-- #main -->
	</div><!-- #primary -->
	

<?php get_footer(); ?>
