<?php
/* RCCAP REPO */
//Create DB table
function repository_db_setup() {
	global $wpdb;
	//global $jal_db_version;
	// print 'foo';die();
	$table_name = $wpdb->prefix . 'repository_items';
	
	if($wpdb->get_var("SHOW TABLES LIKE '$table_name'") != $table_name) {
     	//table not in database. Create new table
		$charset_collate = $wpdb->get_charset_collate();
		$sql = "CREATE TABLE $table_name (
			id mediumint(9) NOT NULL AUTO_INCREMENT,
			user_id bigint(20) NOT NULL,
			title text NULL,
			metadata_json text NULL,
			status varchar(50) NULL,
			url varchar(255) DEFAULT '' NULL,
			container_id varchar(255) DEFAULT '' NULL,
			created_at TIMESTAMP DEFAULT CURRENT_TIMESTAMP NOT NULL,
			updated_at TIMESTAMP DEFAULT CURRENT_TIMESTAMP NOT NULL,
			PRIMARY KEY  (id)
		) $charset_collate;";

		// print $sql;die();

		require_once( ABSPATH . 'wp-admin/includes/upgrade.php' );
		dbDelta( $sql );
	}
}
// add_action( 'admin_menu', 'repository_db_setup' );
// 


add_action( 'wp_ajax_repository_get_item', 'repository_action_get_item' );
function repository_action_get_item() {
	
	$item_id = trim( $_REQUEST['item_id'] );
	if (!strlen($item_id)):
		die( "No Item ID given." );	
	endif;
	
	repository_getItem($item_id);	
	die();
}

add_action( 'wp_ajax_repository_delete_item', 'repository_action_delete_item' );
function repository_action_delete_item() {
	
	$item_id = trim( $_REQUEST['item_id'] );
	if (!strlen($item_id)):
		die( "No Item ID given." );	
	endif;
	
	

	$container_id = trim( $_REQUEST['container_id'] );
	if (strlen($container_id)):
		// print $container_id;
		$metadata = [];
		$metadata['container_id'] = $container_id;
		repository_remove_item_from_index($metadata);	
	endif;

	repository_removeItem($item_id);
	die();
}

add_action( 'wp_ajax_repository_add_item', 'repository_action_add_item' );
function repository_action_add_item() {
	global $current_user;
	$metadata_json = stripslashes( $_POST['metadata'] );
	if (!strlen($metadata_json)):
		die( "No metadata given." );	
	endif;
	//convert to php object to access properties
	$metadata_obj = json_decode($metadata_json,true);
	//get current user id
    $user_id = get_current_user_id();
	
	//todo: add url to item
	$url = $metadata_obj['slug'];
	
	// print_r($metadata_obj);
	// die();
	$item_id = false;
	$errors = false;

	if ($metadata_obj['database_id']){
		//do update
		
		$item_id = $metadata_obj['database_id'];

		
		if (repository_updateItem($user_id,$metadata_json,$metadata_obj['title'],$metadata_obj['status'],$url,$metadata_obj['container_id'],$metadata_obj['database_id'])){
			// print 'ERROR';
			$errors = false;
		} else {			
			$errors = true;
		}
	} else {
		//do insert
		
		$item_id = repository_addItem($user_id,$metadata_json,$metadata_obj['title'],$metadata_obj['status'],$url,$metadata_obj['container_id']);
	}

	if ($metadata_obj['status'] == 'Published' && $item_id && !$errors){
		//Index Item
		
		$metadata_obj['database_id'] = $item_id;
		// $metadata_obj['item-id'] = $item_id;

		repository_index_item($metadata_obj);
	} else {
		//remove it from index
		repository_remove_item_from_index($metadata_obj);
	}

	$response = [];
	$response['status'] = 'error';
	$response['message'] = 'Error saving item';
	$response['id'] = '';
	if ($item_id && !$errors){
		$response['status'] = 'success';
		$response['message'] = 'Item saved.';
		$response['id'] = $item_id;
	} 
	wp_send_json( $response );
	die();

}

function repository_addItem($user_id,$metadata_json,$title, $status,$url,$container_id){
	//save item to DB
	global $wpdb;
	$table_name = $wpdb->prefix . 'repository_items';
	
	$sql = "INSERT INTO $table_name (user_id,metadata_json,title,status,url,container_id,created_at) VALUES(
		".$user_id.",
		'".$wpdb->_real_escape($metadata_json)."',
		'".$title."',
		'".$status."',
		'".$url."',
		'".$container_id."',
		null)";
	// print $sql;
	// die();

	if ($wpdb->query( $sql )){
		return $wpdb->insert_id;
	} 

	return false;
}

function repository_updateItem($user_id,$metadata_json,$title='', $status='draft',$url='',$container_id='',$item_id){
	//save item to DB
	global $wpdb;
	$table_name = $wpdb->prefix . 'repository_items';

	$update_data = [];
	$update_data['user_id'] = $user_id;
	$update_data['metadata_json'] = $metadata_json;
	$update_data['title'] = $title;
	$update_data['status'] = $status;
	$update_data['url'] = $url;
	$update_data['container_id'] = $container_id;
	$update_data['updated_at'] = current_time( 'mysql' );

	$where = [];
	$where['id'] = $item_id;


	// $response = [];
	// $response['status'] = 'error';
	// $response['message'] = 'Error updating item.';
	// $response['id'] = $item_id;

	// print $wpdb->update( $table_name,$update_data,$where );


	// if ($wpdb->query( $sql )){
	if ($wpdb->update( $table_name,$update_data,$where ) !== false){
		// print 'doodoo';die();
		return true;
		// $response['status'] = 'success';
		// $response['message'] = 'Item Updated';
		// $response['id'] = $item_id;
	}  else {
		// $response['message'] = 'Error updating item: '.$wpdb->last_query;
	}
	return false;
}

function repository_index_item($metadata){
	// print_r($metadata);
	
	$index_url = "http://vm-203-101-228-92.qld.nectar.org.au/prototype/indexitem";
	// print $index_url;

	                                                           
                                                                                                                     
	$ch = curl_init($index_url);                                                                      
	curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");                                                                     
	curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($metadata));                                                                  
	curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);                                                                      
	curl_setopt($ch, CURLOPT_HTTPHEADER, array(                                                                          
	    'Content-Type: application/json',                                                                                
	    'Content-Length: ' . strlen(json_encode($metadata)))                                                                       
	);     
	
    $response = curl_exec($ch);

    // print($response);
}

function repository_remove_item_from_index($metadata){
	// print_r($metadata);
	
	$index_url = "http://vm-203-101-228-92.qld.nectar.org.au/prototype/deleteitem";

	                                                           
                                                                                                                     
	$ch = curl_init($index_url);                                                                      
	curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");                                                                     
	curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($metadata));                                                                  
	curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);                                                                      
	curl_setopt($ch, CURLOPT_HTTPHEADER, array(                                                                          
	    'Content-Type: application/json',                                                                                
	    'Content-Length: ' . strlen(json_encode($metadata)))                                                                       
	);     
	
    $response = curl_exec($ch);

    // print($response);
}

function repository_getItem($item_id){
	//get item from DB
	global $current_user, $wpdb;
	$table_name = $wpdb->prefix . 'repository_items';
	//get current user id
    $user_id = get_current_user_id();
    //todo - if admin list all
    $items = $wpdb->get_results( 
		"
		SELECT *
		FROM $table_name
		WHERE id = $item_id
		LIMIT 1
		"
	);

    $response = [];
	$response['status'] = 'error';
	$response['message'] = 'Error loading item.';
	$response['id'] = $item_id;

    if (count($items)){
    	$response['status'] = 'success';
		$response['message'] = 'Item Loaded';
		$response['item'] = $items[0];
    } 
	wp_send_json( $response );
	die();
}

function isUserRepositoryUser(){
	global $current_user;
	if (repository_is_user_repository_admin() ){
		return true;
	}
	$user_id = get_current_user_id();
	$oUserAccessManager = new UserAccessManager();
    // $oCurrentUser = $oUserAccessManager->getCurrentUser();
    $aUserGroupsForObject = $oUserAccessManager->getAccessHandler()->getUserGroupsForObject('user', $user_id);
    foreach($aUserGroupsForObject as $g){
    	if ($g->getGroupName() == "Repository Contributor" ){
    		return true;
    	}
    }
    return false;
}

function repository_is_user_repository_admin(){
	global $current_user;
	$user_id = get_current_user_id();
	$oUserAccessManager = new UserAccessManager();
	return $oUserAccessManager->getAccessHandler()->userIsAdmin($user_id);
}

function repository_getItemList(){
	//get item list for user
	global $current_user, $wpdb;
	$user_id = get_current_user_id();
	
    if (!isUserRepositoryUser()) {
        return;
    }




	$table_name = $wpdb->prefix . 'repository_items';
	//get current user id
    $items = [];
    if (!repository_is_user_repository_admin()) {
	    $items = $wpdb->get_results( 
			"
			SELECT *
			FROM $table_name
			WHERE user_id = $user_id
			"
		);
	} else {
		
		$items = $wpdb->get_results( 
			"
			SELECT *
			FROM $table_name
			"
		);

	}

	return $items;


}

function repository_removeItem($item_id){
	global $wpdb;
	$table_name = $wpdb->prefix . 'repository_items';

	$sql = "DELETE from $table_name WHERE id = $item_id";
	
	$response = [];
	$response['status'] = 'error';
	$response['message'] = 'Error deleting item.';
	$response['id'] = $item_id;

    if ( $wpdb->query( $sql ) ){
    	$response['status'] = 'success';
		$response['message'] = 'Item Deleted';
		$response['item'] = '';
    } 
	wp_send_json( $response );
	die();
}




add_action('admin_menu', 'repository_menu');

function repository_menu() {
	add_users_page('Repository Users', 'Repository: My Items', 'read', 'my_items', 'page_my_items');
	add_users_page('Repository Users', 'Repository: Add New Item', 'read', 'add_item', 'page_add_item');
}

function page_my_items(){
	// print 'My Items';
	if (!isUserRepositoryUser()) {
        return;
    }
	if ( '' === locate_template( 'repository_my_items.phtml', true, false ) ){
		include( 'repository_my_items.phtml' );	
	}
}

function page_add_item(){
	//print 'Add new Item';
	if (!isUserRepositoryUser()) {
        return;
    }
	if ( '' === locate_template( 'repository_add_new.phtml', true, false ) ){
		include( 'repository_add_new.phtml' );	
	}

}


add_action( 'wp_ajax_repository_set_cookie_disclaimer', 'repository_set_cookie_disclaimer' );
add_action( 'wp_ajax_nopriv_repository_set_cookie_disclaimer', 'repository_set_cookie_disclaimer' );

function repository_set_cookie_disclaimer(){
    setcookie( 'site_disclaimer',$_POST['seen_disclaimer'], 366 * DAYS_IN_SECONDS, COOKIEPATH, COOKIE_DOMAIN );
    return true;

    exit();
}