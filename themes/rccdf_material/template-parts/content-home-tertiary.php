<?php
/**
 * The template used for displaying page content in page.php
 *
 * @package MDLWP
 */

?>


	<section class="mdl-grid home-tertiary">
		<div class="mdl-cell mdl-cell--1-col"></div>
		<div class="mdl-cell mdl-cell--10-col contribute-box">
			<div class="mdl-grid">
				<div class="mdl-cell mdl-cell--3-col mdl-cell--12-col-tablet icon-box">
					<img src="<?php echo get_stylesheet_directory_uri(); ?>/images/upload-icon.png"/>
				</div>
				<div class="mdl-cell mdl-cell--9-col mdl-cell--12-col-tablet">
					<h3>Contribute to the RCCAP Repository</h3>
					<p>The RCCAP provides consortium members with a data storage services, which uses climate change specific metadata to ensure that all your content is well described, discoverable and able to be re-used.</p>
					<a class="mdl-button mdl-button--colored mdl-button--colored-secondary mdl-js-button mdl-js-ripple-effect" href="/adb/login/register/">Join RCCAP</a>
				</div>
			</div>
			
		</div>
		<div class="mdl-cell mdl-cell--1-col"></div>

		<p class="link-to-top"><a href="#" class="mdl-button mdl-button--colored"><i class="fa fa-chevron-circle-up"></i> Back to top</a></p>
	</section>



