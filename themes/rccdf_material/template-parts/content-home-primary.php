<?php
/**
 * The template used for displaying page content in page.php
 *
 * @package MDLWP
 */

?>

<section class="mdl-grid mdl-grid--no-spacing home-primary mdl-shadow--2dp "> 


		<?php $args = array( 'post_type' => 'home-primary', 'posts_per_page' => 3, 'order-by' => 'date' );
			$loop = new WP_Query( $args );
			while ( $loop->have_posts() ) : $loop->the_post();  ?>

			<?php $icon = get_post_meta($post->ID, 'Icon', true); ?>
			<?php $link = get_post_meta($post->ID, 'CTA-link', true); ?>
			<?php $host = get_site_url(); ?>
			<?php $image = wp_get_attachment_image_src( get_post_thumbnail_id( $post->ID ), 'full' ); ?>


		 	<div class="mdl-card mdl-cell mdl-cell--4-col mdl-cell--8-col-tablet mdl-cell--12-col-phone">
			  <div class="mdl-card__title" style="background-image:url('<?php echo $image[0]; ?>');">
			    <h2 class="mdl-card__title-text">
			  		<a href="<?php echo $host; ?><?php echo $link; ?>">
			    		<?php the_title(); ?>
			    	</a>
			    </h2>
			  </div>
			  <div class="mdl-card__supporting-text">
				<?php the_content(); ?>
			  </div>
			  <div class="mdl-card__menu">
			    <a class="mdl-button mdl-js-button mdl-button--fab mdl-js-ripple-effect mdl-button--colored" href="<?php echo $host; ?><?php echo $link; ?>">
				  <i class="fa fa-<?php echo $icon; ?>"></i>
				</a>
			  </div>
			 </div>
		<?php endwhile; ?>


</section> <!-- .mdl-cell -->


