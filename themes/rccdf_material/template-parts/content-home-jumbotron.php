<?php
/**
 * The template used for displaying page content in page.php
 *
 * @package MDLWP
 */

?>

<section class="mdl-grid home-jumbotron"> 


		<?php $args = array( 'post_type' => 'jumbotron', 'posts_per_page' => 1, 'order-by' => 'date' );
			$loop = new WP_Query( $args );
			while ( $loop->have_posts() ) : $loop->the_post();  ?>

			<?php $buttonText = get_post_meta($post->ID, 'button-text', true); ?>
			<?php $link = get_post_meta($post->ID, 'button-link', true); ?>
			<?php $host = get_site_url(); ?>
			<?php $image = wp_get_attachment_image_src( get_post_thumbnail_id( $post->ID ), 'full' ); ?>


		 	<div class="mdl-cell mdl-cell--1-offset-desktop mdl-cell--10-col mdl-jumbotron">
			 	<div class="mdl-grid">
				  <div class="mdl-cell mdl-cell--5-col">
				    <h1><?php the_title(); ?></h1>
				    <a href="<?php echo $host.'/'.$link; ?>"><button class="mdl-button mdl-button--colored mdl-js-button mdl-button--raised mdl-js-ripple-effect"><?php echo $buttonText; ?> <i class="fa fa-chevron-circle-right"></i></button></a>
				  
				  </div>
				  <div class="mdl-cell mdl-cell--7-col">
				    <?php the_content(); ?>
				    </div>
				 </div>
			</div>
		<?php endwhile; ?>


</section> <!-- .mdl-cell -->


