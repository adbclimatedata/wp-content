<?php
/**
 * The template used for displaying single articles
 *
 * @package MDLWP
 */

?>

<?php
    // Gets the stored background color value 
    $color_value = get_post_meta( get_the_ID(), 'mdlwp-ribbon-bg-color', true ); 
    // Checks and returns the color value
  	$color = (!empty( $color_value ) ? 'background-color:' . $color_value . ';' : '');

  	// Gets the stored height value 
    $height_value = get_post_meta( get_the_ID(), 'mdlwp-ribbon-height', true ); 
    // Checks and returns the height value
  	$height = (!empty( $height_value ) ? 'height:' . $height_value . ';' : 'height:180px;');

  	 // Gets the uploaded featured image
  	$featured_img = wp_get_attachment_image_src( get_post_thumbnail_id( $post->ID ), 'full' );
  	// Checks and returns the featured image
  	$bg = (!empty( $featured_img ) ? "background-image: url('". $featured_img[0] ."');" : "background: #3e6371;");
?>

<div class="mdl-cell mdl-cell--12-col mdl-card mdl-shadow--2dp"> 
	
		<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
				<div class=" mdl-card__media" style="<?php echo $color . $bg . $height; ?> ">
					<header>
						<?php the_title( sprintf( '<h3>','</h3>' )); ?>
					</header><!-- .entry-header -->
				</div>
				<div class=" mdl-card__supporting-text">
					<div class="entry-content">
						<?php the_content(); ?>
						<?php
							wp_link_pages( array(
								'before' => '<div class="page-links">' . esc_html__( 'Pages:', 'mdlwp' ),
								'after'  => '</div>',
							) );
						?>
					</div><!-- .entry-content -->
				</div>
	</article><!-- #post-## -->
</div> <!-- .mdl-cell -->

