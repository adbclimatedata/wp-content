<?php
/**
 * searchform.php
 *
 * Search for main menu.
 *
 *
 */
?>
<?php do_action( 'mdlwp_before_searchform' ); ?>
<form role="search" method="get" class="search-form" action="<?php echo home_url( '/' ); ?>">
	<div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label">
		<input class="mdl-textfield__input" type="text" value="<?php echo get_search_query() ?>" name="s" id="fixed-header-drawer-exp" />
		<label class="mdl-textfield__label" for="fixed-header-drawer-exp">
		    Search Terms
		</label>
		<button class="button mdl-button mdl-js-button" type="submit"><i class="fa fa-search"></i></button>
	</div>
</form>
<?php do_action( 'mdlwp_after_searchform' ); ?>