<?php
/**
 * The template used for displaying page content in page.php
 *
 * @package MDLWP
 */

?>

<section class="home-latest-submission mdl-cell mdl-cell--4-col mdl-cell--12-col-tablet">
	<div class="mdl-card mdl-shadow--2dp">
	  <div class="mdl-card__title">
	    <h2 class="mdl-card__title-text">Latest Submissions</h2>
	  </div>
	</div>	

	
	  <?php $args = array( 'post_type' => 'data', 'posts_per_page' => 3 );
			//$loop = new WP_Query( $args );
			$items = rccap_get_recent_submissions();
			foreach($items->hits->hits as $item):  ?>
			 <a href="/prototype/item/<?php echo $item->_id; ?>" title="<?php echo $item->_source->title; ?>" class="submission-link mdl-js-ripple-effect">
				<div class="mdl-card mdl-shadow--2dp">
					<div class="mdl-card__supporting-text mdl-grid">

					  <?php $type = $item->_source->content_type; ?>

					  <div class="mdl-cell mdl-cell--3-col">
					  	<a class="mdl-button mdl-js-button mdl-button--fab mdl-js-ripple-effect mdl-button--colored custom 
						  <?php if ( $type == 'Tool' ) : ?> 
						  	salmon"><i class="fa fa-wrench"></i>
						  <?php elseif ( $type == 'Collection' ) : ?>
						  	lblue"><i class="fa fa-folder-open"></i>
						  <?php elseif ( $type == 'Dataset' ) : ?>
						  	lbrown"><i class="fa fa-table"></i>
						   <?php else:  ?>	
						   	lblue"><i class="fa fa-folder-open"></i>
						  <?php endif; ?>
						  </a>
						</div>
						<div class="mdl-cell mdl-cell--9-col">
							<a href="/prototype/item/<?php echo $item->_id; ?>" title="<?php echo $item->_source->title; ?>" class="">
						  	<h3 class="sans-serif"><?php echo $item->_source->title; ?></h3>
						  </a>
						  <h5 class="sans-serif post-meta"><?php echo $item->_source->timestamp; ?></h5>
						  <?php echo substr($item->_source->{'short-summary'},0,150); ?>
						</div>
					</div>
				</div>
			 </a>
			<?php endforeach; ?>

	<div class="mdl-card mdl-shadow--2dp call-to-action">
	  <div class="mdl-card__title">
	    <h4 class="mdl-card__title-text sans-serif">Explore The Repository</h4>
	  </div>
	  <div class="mdl-card__menu">
	    <a href="/prototype/" class="mdl-button mdl-js-button mdl-button--fab mdl-js-ripple-effect mdl-button--colored">
		  <i class="fa fa-chevron-right"></i>
		</a>
	  </div>
	</div>
	 
	
</section>


