<?php
/**
 * The template for displaying archive pages.
 *
 * Learn more: http://codex.wordpress.org/Template_Hierarchy
 *
 * @package MDLWP
 */

get_header(); ?>

		
	<div id="primary" class="mdl-grid content-area">
		<main id="main" class="site-main mdl-grid mdlwp-900" role="main">

		<?php if ( have_posts() ) : ?>
			<div class="mdl-cell mdl-cell--12-col">
				<div class="mdl-card__media" style="height:180px;">
					<header class="page-header">
						<h1 class="page-title">Latest News</h1>
						<?php
							the_archive_description( '<div class="taxonomy-description">', '</div>' );
							echo get_post_format();
						?>
					</header><!-- .page-header -->
				</div>
			</div>

			<?php /* Start the Loop */ ?>
			<?php while ( have_posts() ) : the_post(); ?>

				<?php

					/*
					 * Include the Post-Format-specific template for the content.
					 * If you want to override this in a child theme, then include a file
					 * called content-___.php (where ___ is the Post Format name) and that will be used instead.
					 */
					get_template_part( 'template-parts/content', 'news' );
				?>

			<?php endwhile; ?>

			<?php the_posts_navigation(); ?>

		<?php else : ?>

			<?php get_template_part( 'template-parts/content', 'none' ); ?>

		<?php endif; ?>

		</main><!-- #main -->
	</div><!-- #primary -->

<?php get_footer(); ?>
