<?php
/**
 * The template for displaying 404 pages (not found).
 *
 * @package MDLWP
 */

get_header(); ?>

		
	<div id="primary" class="content-area">
		<main id="main" class="site-main mdl-grid mdlwp-900" role="main">

			<section class="error-404 not-found mdl-cell mdl-cell--12-col mdl-card mdl-shadow--2dp">
				<div class="mdl-card__media">
					<header class="page-header">
						<h1 class="page-title"><?php esc_html_e( 'That page can&rsquo;t be found.', 'mdlwp' ); ?></h1>
					</header><!-- .page-header -->
				</div>
				<div class="page-content mdl-card__supporting-text">
					<p><?php esc_html_e( 'It looks like nothing was found at this location.', 'mdlwp' ); ?></p>

					<?php the_widget( 'WP_Widget_Recent_Posts' ); ?>

					<?php the_widget( 'WP_Widget_Tag_Cloud' ); ?>

				</div><!-- .page-content -->
				<div class="mdl-card__actions mdl-card--border">
			    <a class="mdl-button mdl-button--colored mdl-js-button mdl-js-ripple-effect" href="<?php echo get_site_url(); ?>">
			      <i class="fa fa-chevron-circle-left"></i> Return Home
			    </a>
			  </div>
			</section><!-- .error-404 -->

		</main><!-- #main -->
	</div><!-- #primary -->

<?php get_footer(); ?>
